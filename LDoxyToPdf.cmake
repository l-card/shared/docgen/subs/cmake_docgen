# Converts a CMake list to a string containing elements separated by spaces
function(TO_LIST_SPACES _LIST_NAME OUTPUT_VAR)
  set(NEW_LIST_SPACE)
  foreach(ITEM ${${_LIST_NAME}})
    set(NEW_LIST_SPACE "${NEW_LIST_SPACE} ${ITEM}")
  endforeach()
  string(STRIP ${NEW_LIST_SPACE} NEW_LIST_SPACE)
  set(${OUTPUT_VAR} "${NEW_LIST_SPACE}" PARENT_SCOPE)
endfunction()



function(ADD_DOXY_TO_PDF_TARGET TEXFILE)
  # Если нашли Doxygen - создаем цель для сборки документации
  find_package(Doxygen)
  if(DOXYGEN_FOUND)
    file(COPY ${DOC_DOXY_STY_FILE} DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/doc/pdf)
    file(COPY ${PDF_IMG_FILES} DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/doc/pdf)

    TO_LIST_SPACES(DOXYGEN_INPUT_FILES DOXYGEN_INPUT_FILES_STRING)

    if(NOT DOC_DOXYGEN_PRJ_FILE)
        set(DOC_DOXYGEN_PRJ_FILE ${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile.in)
    endif(NOT DOC_DOXYGEN_PRJ_FILE)

    configure_file(${DOC_DOXYGEN_PRJ_FILE}
                   ${CMAKE_CURRENT_BINARY_DIR}/doc/Doxyfile @ONLY)
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/doc/config.xml.in
                   ${CMAKE_CURRENT_BINARY_DIR}/doc/pdf/config.xml @ONLY)

    set(DOXYGEN_INPUT_FILES ${DOXYGEN_INPUT_FILES} ${DOC_DOXYGEN_PRJ_FILE} doc/config.xml.in)



    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/doc/html/index.html
        COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/doc/Doxyfile
        DEPENDS ${DOXYGEN_INPUT_FILES} ${PDF_IMG_FILES}
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        COMMENT "Generating API documentation with Doxygen" VERBATIM
    )

    message(STATUS "xmlToTex:  ${DOXY_XMLTOTEX_EXECUTABLE} (bindir=${CMAKE_CURRENT_BINARY_DIR}")
    add_custom_target(${TEXFILE}_upd_tex
        ${DOXY_XMLTOTEX_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/doc/pdf/config.xml
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/doc
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/doc/html/index.html ${PDF_IMG_FILES} ${TEX_DEPENDS_FILES}
        DEPENDS doxyXmlToLatex
        COMMENT "Update tex documentation from doxy xml output" VERBATIM
    )

    find_package(LATEX)
    message(STATUS "pdflatex:  ${PDFLATEX_COMPILER}")
    if(PDFLATEX_COMPILER)
        include(${CMAKE_USE_LATEX_FILE})
        set(LATEX_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR}/doc/pdf)
        ADD_LATEX_DOCUMENT(doc/${TEXFILE}.tex DEPENDS ${TEXFILE}_upd_tex ${DOXYGEN_INPUT_FILES} ${TEX_DEPENDS_FILES}
            MANGLE_TARGET_NAMES)
    endif(PDFLATEX_COMPILER)
  endif(DOXYGEN_FOUND)
endfunction(ADD_DOXY_TO_PDF_TARGET)
  
